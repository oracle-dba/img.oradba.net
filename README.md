# [img.oradba.net](https://img.oradba.net) source codes

<br/>

### Run img.oradba.net on localhost

    # vi /etc/systemd/system/img.oradba.net.service

Insert code from img.oradba.net.service

    # systemctl enable img.oradba.net.service
    # systemctl start img.oradba.net.service
    # systemctl status img.oradba.net.service

http://localhost:4067
